package no.uib.inf101.datastructure;

import java.awt.Color;

public interface IGrid<T> extends GridDimension, GridCellCollection<T> {

  /**
   * Get the elem of the cell at the given position.
   *
   * @param pos the position
   * @return the elem of the cell
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
  T get(CellPosition pos);

  /**
   * Set the elem of the cell at the given position.
   *
   * @param pos the position
   * @param color the new elem
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
  void set(CellPosition pos, T color);

}